package com.itheima.dao;

import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;

public interface CheckItemDao {
    void addCheckItems(CheckItem checkItem);
}

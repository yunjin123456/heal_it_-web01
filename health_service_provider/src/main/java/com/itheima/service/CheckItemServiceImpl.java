package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.common.MessageConst;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
public class CheckItemServiceImpl implements CheckItemService {
    @Autowired
    private CheckItemDao checkItemDao;
    @Override
    public Result addCheckItems(CheckItem checkItem) {

        try {
             checkItemDao.addCheckItems(checkItem);
             return new Result(true,MessageConst.ADD_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return   new Result(false, "添加检查项失败");

        }
    }

}

package com.itheima.exception;

public class BusinessRuntimeException extends RuntimeException {
    public BusinessRuntimeException(String message){
        super(message);
    }
}

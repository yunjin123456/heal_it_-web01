package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("checkItems")
@RestController
public class CheckItemsController {


    @Reference
    CheckItemService checkItemService;
    @RequestMapping("/addCheckItems")
    public Result addCheckItems(@RequestBody CheckItem checkItem){
        log.debug("checkItem 检查项 ：" +checkItem);
        Result result = checkItemService.addCheckItems(checkItem);
        return  result;
    }
    @RequestMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        log.debug("queryBean"+queryPageBean);
        return null;
    }
}
